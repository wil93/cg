#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glui.h>

const int screenWidth  = 800;
const int screenHeight = 800;

GLUI_Panel* options_panel;
GLUI_RadioGroup* options_radio;

int winId = 1;

struct GLPoint2D {
  GLint x, y;
};

std::vector<GLPoint2D> punti;

int mode = 0; // 0 = inserimento, 1 = modifica
int derivative = 0; // 1 = modifico la derivata, 0 = non la modifico
int selected = -1;

void myMouse(int button, int state, GLint mouse_x, GLint mouse_y) {
  fprintf(stderr, "myMouse: %d, %d\n", mouse_x, mouse_y);
  GLPoint2D newPoint = {mouse_x, screenHeight - mouse_y};
  double tolerance = 10.0; // la prof. dice 3.0

  if (state == GLUT_DOWN) {
    switch (button) {
    case GLUT_LEFT_BUTTON:
      /* Premuto il tasto sinistro
       */
      if (mode == 1 || derivative == 1) {
        if (!punti.empty()) {
          selected = 0;
          double distance = sqrt(pow(punti[0].x - newPoint.x, 2) +
                                 pow(punti[0].y - newPoint.y, 2));
          fprintf(stderr, "Distanza: %.10f\n", distance);
          for (size_t i=1; i<punti.size(); i++) {
            double new_d = sqrt(pow(punti[i].x - newPoint.x, 2) +
                                pow(punti[i].y - newPoint.y, 2));
            fprintf(stderr, "New d: %.10f\n", new_d);
            if (new_d < distance) {
              distance = new_d;
              selected = i;
            }
          }
          if (distance > tolerance) {
            selected = -1;
          }
          fprintf(stderr, "Selected %d\n", selected);
        }
      } else if (mode == 0) {
        //~ glBegin(GL_POINTS);
          //~ glVertex2f(newPoint.x, newPoint.y);
        //~ glEnd();
        //            `-- roba inutile?

        punti.push_back(newPoint);
        fprintf(stderr, "%lu\n", punti.size());
      }
      break;

    case GLUT_RIGHT_BUTTON:
      /* Premuto il tasto destro
       */

      punti.clear();
      break;

    case GLUT_MIDDLE_BUTTON:
      /* Premuto il tasto centrale
       */

      punti.pop_back();
      break;
    }
  } else {  // state == GLUT_UP
    switch (button) {
    case GLUT_LEFT_BUTTON:
      selected = -1;
      break;
    }
  }
  glutPostRedisplay();
}

void mouseMove(GLint mouse_x, GLint mouse_y) {
  GLPoint2D newPoint = {mouse_x, screenHeight - mouse_y};

  fprintf(stderr, "Well, %d\n", selected);
  if (selected != -1) {
    if (mode == 1) {
      punti[selected] = newPoint;
    }
  }
}

void display() {
  glClearColor(1.0, 1.0, 1.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);
  glBegin(GL_LINE_STRIP);
  for (auto p: punti) {
    glVertex2f(p.x, p.y);
  }
  glEnd();
  glFlush();
}

void myinit() {
  glClearColor(1.0, 1.0, 1.0, 0.0);
  glColor3f(1.0, 0.0, 0.0);
  glPointSize(4.0);
  glMatrixMode(GL_PROJECTION);  // definisce il sistema di coordinate
  glLoadIdentity();
  gluOrtho2D(0.0, screenWidth, 0.0, screenHeight);
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(screenWidth, screenHeight);
  glutInitWindowPosition(100, 150);
  glutCreateWindow("Curve 2D");

  glutDisplayFunc(display);
  glutMouseFunc(myMouse);
  glutMotionFunc(mouseMove);

  GLUI* glui = GLUI_Master.create_glui("Opzioni");
  options_panel = glui->add_panel("Opzioni2", GLUI_PANEL_EMBOSSED);
  options_radio = glui->add_radiogroup_to_panel(options_panel, &mode);
  glui->add_radiobutton_to_group(options_radio, "Aggiungi punti");
  glui->add_radiobutton_to_group(options_radio, "Modifica punti");

  glui->set_main_gfx_window(winId);
  myinit();
  glutMainLoop();
}
