#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <vector>
#include <limits>
#include <algorithm>
#include <iostream>
#ifdef __linux__
#include <GL/freeglut.h>
#include <GL/glut.h>
#include <GL/glui.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <cmath>
#else
#define NOMINMAX
#include <Windows.h>
#include <gl/glut.h>
#include <gl/glui.h>
#include <gl/GLU.h>
#include <gl/GL.h>
#define _USE_MATH_DEFINES
#include <math.h>
#endif

/* Alcune costanti utili */

static int WINDOW_WIDTH;
static int WINDOW_HEIGHT;

static const float TOLERANCE = 8;
static const float EPSILON   = 1e-8;
static const int MAX_MULT    = 4;
static const int MAX_DEGREE  = 100;
static const int TSAMPLES    = 1000;  // numero di valori di t in cui valutare la curva interpolante
static const float TSTEP     = 1.0f / (TSAMPLES-1);

struct GLColor3f {
  GLfloat r, g, b;
};

/* I vari colori utilizzati */

static const GLColor3f BACKGROUND_COLOR     = {1, 1, 1};
static const GLColor3f SELECTED_POINT_COLOR = {1, 0.5, 0};
static const GLColor3f INSERT_POINT_COLOR   = {0.5, 1, 0.5};
static const GLColor3f MULT_POINT_COLOR     = {0.7, 0.7, 1};
static const GLColor3f POINT_COLOR          = {0.8, 0.8, 0.8};
static const GLColor3f CONTROL_LINE_COLOR   = {0.8, 0.8, 0.8};
static const GLColor3f CURVE_COLOR          = {0, 0.7, 0};
static const GLColor3f SUBDIVISION_COLOR    = {1, 0, 0.5};
static const GLColor3f DEGREE_COLOR         = {1, 0.5, 0};
static const GLColor3f BASE_COLOR           = {1, 0, 1};
static const GLColor3f MULTIPLICITY_COLOR   = {0, 1, 0};
static const GLColor3f DELTA_HELPER_COLOR   = {1, 0.4, 0};

/* Definizione del punto */

struct GLPoint2D {
  GLfloat x, y;

  GLPoint2D(float x, float y) : x(x), y(y) {}
  GLPoint2D() : x(0), y(0) {}

  GLPoint2D operator + (const GLPoint2D& other) const {
    return GLPoint2D(x + other.x, y + other.y);
  }

  GLPoint2D operator - (const GLPoint2D& other) const {
    return GLPoint2D(x - other.x, y - other.y);
  }

  GLPoint2D operator * (const GLfloat& scalar) const {
    return GLPoint2D(scalar * x, scalar * y);
  }

  GLfloat operator * (const GLPoint2D& other) const {
    return x * other.x + y * other.y;
  }

  bool operator < (const GLPoint2D& other) const {
    if (x != other.x) {
      return x < other.x;
    } else {
      return y < other.y;
    }
  }
};

/**
 * Definizione del Control Point. E' un punto, con l'aggiunta di altri
 * campi utili che altrimenti andrebbero memorizzati in tanti vector
 * separati.
 */
struct ControlPoint : GLPoint2D {
  float t;             // valore del parametro t in questo punto
  unsigned char mult;  // molteplicita' del punto di controllo
  float weight;        // peso del punto di controllo
  GLPoint2D delta;     // coppia di differenze (mouse_x - x, mouse_y - y)

#ifdef __linux__
  static struct {
#else
  static struct _ {
#endif
    bool operator() (const ControlPoint& cp, const float t) const {
      return cp.t < t;
    }
  } compare_t;

  ControlPoint(float x, float y) : GLPoint2D(x, y), mult(1), weight(1), delta() {}
  ControlPoint(GLPoint2D point) : GLPoint2D(point), mult(1), weight(1), delta() {}
  ControlPoint() : GLPoint2D(), mult(1), weight(1), delta() {}
};

#ifndef __linux__
ControlPoint::_ ControlPoint::compare_t;
#endif

/* Funzioni per l'interpolazione di Hermite */

static inline float phi0(const float t) {
  return 2 * t * t * t - 3 * t * t + 1;
}

static inline float phi1(const float t) {
  return t * t * t - 2 * t * t + t;
}

static inline float psi0(const float t) {
  return - 2 * t * t * t + 3 * t * t;
}

static inline float psi1(const float t) {
  return t * t * t - t * t;
}

std::vector<ControlPoint> points;                   // lista di punti (x, y)
std::vector<ControlPoint> elevated;                 // lista di punti ausiliaria (per la degree elevation)
std::vector<ControlPoint>::iterator selected;       // iteratore al punto di controllo selezionato
std::vector<ControlPoint>::iterator insert_before;  // iteratore al punto prima del quale inserire un nuovo punto
GLPoint2D insert_point;                             // quando insert_before e' valido, e' il punto da inserire

struct Options {
  int drag_point;    // 1 = trascino il punto, 0 = non trascino
  int param;         // 1 = parametrizzazione corde, 0 = uniforme
  int method;        // 1 = hermite, 2 = bezier, 3 = spline
  int save_delta;    // 1 = salvo delta mouse, 0 = non salvo
  float subdiv;      // valore t in cui mostrare suddivisione: [0, 1]
  int show_subdiv;   // 1 = mostro la suddivisione, 0 = non la mostro
  int degree;        // valore degree elevation [0, MAX_DEGREE]
  int edit_weight;   // 1 = modifico i pesi, 0 = non li modifico
  int edit_mult;     // 1 = modifico la molteplicita', 0 = non la modifico
  int mult_index;    // indice del nodo al quale modificare la molteplicita'
  int new_mult;      // nuova molteplicita' da assegnare al nodo mult_index
  GLColor3f line_color;
  GLColor3f bg_color;
  
  Options() {
    drag_point = 0;
    param = 0;
    method = 0;
    save_delta = 0;
    subdiv = 0;
    degree = 0;
    edit_weight = 0;
    edit_mult = 0;
    mult_index = 0;
    new_mult = 0;
  }
} options;

static int finestra, basi;
static GLUI_Spinner *spinner_deg, *spinner_index, *spinner_mult;

bool cmp(ControlPoint a, ControlPoint b) {
  return a.t < b.t;
}

int localizza_intervallo_internodale(float target_t) {
  // Ricerca binaria del valore target_t nell'array dei punti
  return std::lower_bound(points.begin(), points.end(), target_t + EPSILON, ControlPoint::compare_t) - points.begin() - 1;
}

void trigger_redisplay() {
  glutSetWindow(finestra);
  glutPostRedisplay();
  glutSetWindow(basi);
  glutPostRedisplay();
}

void clear_points(int parameter) {
  assert(parameter == 1);  // no warning
  points.clear();
  trigger_redisplay();
}

void reset_deltas(int parameter) {
  assert(parameter == 1);  // no warning
  for (size_t i=0; i<points.size(); i++) {
    points[i].delta.x = 0;
    points[i].delta.y = 0;
    points[i].weight = 1;
    points[i].mult = (i == 0 || i == points.size() - 1) ? MAX_MULT : 1;
  }
  trigger_redisplay();
}

void mouse_click(int button, int state, int mouse_x, int mouse_y) {
  std::cerr << "mouse_click" << std::endl;
  GLPoint2D new_point(mouse_x, WINDOW_HEIGHT - mouse_y);

  if (state == GLUT_DOWN) {
    switch (button) {
    case GLUT_LEFT_BUTTON: {
      /* Premuto il tasto sinistro
       */
      if (selected == points.end()) {
        if (insert_before != points.end()) {
          // metto un nuovo punto dove richiesto (e lo seleziono)
          selected = points.insert(insert_before, ControlPoint(insert_point));
        } else {
          // metto un nuovo punto dove e' il mouse
          points.push_back(new_point);
          selected = points.begin() + points.size() - 1;
        }
      }
      // comincio a trascinare il punto
      options.drag_point = 1;
      break;
    }
    case GLUT_RIGHT_BUTTON: {
      /* Premuto il tasto destro
       */
      if (selected != points.end()) {
        points.erase(selected);
        selected = insert_before = points.end();
      }
      break;
    }
    case GLUT_MIDDLE_BUTTON:
      /* Premuto il tasto centrale
       */
      if (selected != points.end()) {
        options.save_delta = 1;  // comincio a salvare il delta
      }
      break;
    }
  } else if (state == GLUT_UP) {
    options.drag_point = 0;  // smetto di trascinare il punto
    options.save_delta = 0;  // smetto di salvare il delta
  }

  trigger_redisplay();
}

/**
 * La funzione point_segment_distance(a, b, p) restituisce una coppia
 * formata da:
 *   (1) distanza tra il segmento ab ed il punto p
 *   (2) il punto sul segmento ab che realizza tale distanza
 */
std::pair<float, GLPoint2D> point_segment_distance(const GLPoint2D a, const GLPoint2D b, const GLPoint2D p) {
  const float square_len = pow(a.x - b.x, 2) + pow(a.y - b.y, 2);
  if (square_len < EPSILON) {
    return std::make_pair(hypotf(p.x - a.x, p.y - a.y), a);  // caso a == b
  }
  // Considera la retta che estende il segmento, parametrizzata come
  // a + t(b - a). Cerchiamo la proiezione di p sulla retta.
  // Accadra' quando t = [(p-a) . (b-a)] / |b-a|^2
  const float t = (p - a) * (b - a) / square_len;
  if (t < 0) {
    return std::make_pair(hypotf(p.x - a.x, p.y - a.y), a);  // oltre l'estremo a
  } else if (t > 1) {
    return std::make_pair(hypotf(p.x - b.x, p.y - b.y), b);  // oltre l'estremo b
  }
  const GLPoint2D projection = a + (b - a) * t;  // la proiezione e' sul segmento
  return std::make_pair(hypotf(p.x - projection.x, p.y - projection.y), projection);
}

/**
 * La funzione nearest_neighbor(x, y) restituisce l'iteratore al punto
 * piu' vicino alla posizione (x, y) passata come parametro.
 *
 * Se il punto piu' vicino ha una distanza superiore alla massima
 * distanza tollerata (memorizzata nella costante TOLERANCE) allora
 * viene restituito l'indice NOT_SELECTED.
 *
 * Nota: questa funzione si puo' ottimizzare facendo uso di un 2d-tree,
 * che abbasserebbe la complessita' da O(n) a O(log(n)).
 */
std::vector<ControlPoint>::iterator nearest_neighbor(const int x, const int y) {
  auto iterator = points.end();
  float distance = TOLERANCE;
  for (auto it=points.begin(); it!=points.end(); it++) {
    const float new_d = hypotf(it->x - x, it->y - y);
    if (new_d < distance) {
      distance = new_d;
      iterator = it;
    }
  }
  return iterator;
}

void mouse_move(int mouse_x, int mouse_y) {
  std::cerr << "mouse_move" << std::endl;
  const GLPoint2D new_point(mouse_x, WINDOW_HEIGHT - mouse_y);

  if (options.drag_point == 1) {
    assert(selected != points.end());
    selected->x = new_point.x;
    selected->y = new_point.y;
  } else if (options.save_delta == 1) {
    selected->delta = GLPoint2D(
      mouse_x - selected->x,
      WINDOW_HEIGHT - mouse_y - selected->y
    );
    selected->weight = std::max(0.0f,
        1.0f + (WINDOW_HEIGHT - mouse_y - selected->y) / TOLERANCE * 2);
  } else {
    selected = nearest_neighbor(mouse_x, WINDOW_HEIGHT - mouse_y);
  }

  // Se non sono vicino ad un punto, vedi se sono vicino ad un segmento
  if (selected == points.end()) {
    insert_before = points.end();
    std::pair<float, GLPoint2D> distance = std::make_pair(TOLERANCE, GLPoint2D());
    for (size_t i=1; i<points.size(); i++) {
      auto new_d = point_segment_distance(points[i-1], points[i], new_point);
      if (new_d < distance) {
        distance = new_d;
        insert_before = points.begin() + i;
        insert_point = new_d.second;
      }
    }
  }

  trigger_redisplay();
}

void uniform_parametrization() {
  float step = 1.0 / (points.size() - 1);
  for (size_t i=0; i<points.size(); i++) {
    points[i].t = i * step;
  }
}

void chord_parametrization() {
  points[0].t = 0;
  for (size_t i=1; i<points.size(); i++) {
    points[i].t = hypotf(points[i].x - points[i-1].x, points[i].y - points[i-1].y);
    points[i].t += points[i-1].t;
  }
  for (auto& p: points) {
    p.t /= points.back().t;
  }
}

float dx(int i) {
  if (i == 0) {
    return 0;
  }

  if (fabs(points[i].delta.x) < EPSILON) {
    return (points[i].x - points[i-1].x) / (points[i].t - points[i-1].t);
  } else {
    return points[i].delta.x / (points[i].t - points[i-1].t);
  }
}

float dy(int i) {
  if (i == 0) {
    return 0;
  }

  if (fabs(points[i].delta.y) < EPSILON) {
    return (points[i].y - points[i-1].y) / (points[i].t - points[i-1].t);
  } else {
    return points[i].delta.y / (points[i].t - points[i-1].t);
  }
}

void hermite_interpolation() {
  //glColor3f(CURVE_COLOR.r, CURVE_COLOR.g, CURVE_COLOR.b);
  glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for (float ti=0; ti<=1; ti+=TSTEP) {
    size_t idx = localizza_intervallo_internodale(ti);
    float width = points[idx+1].t - points[idx].t;
    float time = (ti - points[idx].t) / width;
    GLPoint2D ih(
      points[idx].x * phi0(time) + dx(idx) * phi1(time) * width + points[idx+1].x * psi0(time) + dx(idx+1) * psi1(time) * width,
      points[idx].y * phi0(time) + dy(idx) * phi1(time) * width + points[idx+1].y * psi0(time) + dy(idx+1) * psi1(time) * width
    );
    glVertex2d(ih.x, ih.y);
  }
  glEnd();
}

void bezier() {
  std::vector<GLPoint2D> c(points.size());
  std::vector<float> w(points.size());

  //glColor3f(CURVE_COLOR.r, CURVE_COLOR.g, CURVE_COLOR.b);
  glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for (float ti=0; ti<=1; ti+=TSTEP) {
    for (size_t i=0; i<points.size(); i++) {
      c[i].x = points[i].weight * points[i].x;
      c[i].y = points[i].weight * points[i].y;
      w[i] = points[i].weight;
    }
    for (size_t i=1; i<points.size(); i++) {
      for (size_t j=0; j+i<points.size(); j++) {
        c[j].x = c[j].x * (1 - ti) + ti * c[j+1].x;
        c[j].y = c[j].y * (1 - ti) + ti * c[j+1].y;
        w[j] = w[j] * (1 - ti) + ti * w[j+1];
      }
    }
    glVertex2f(c[0].x / w[0], c[0].y / w[0]);
  }
  glEnd();
}

void hermite_base() {
  // Disegnamo, una dopo l'altra, le 4 funzioni
  glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  glBegin(GL_LINE_STRIP);
  for (float ti=0; ti<=1; ti+=TSTEP) {
    float temp = phi0(ti);
    glVertex2d(ti, temp * 0.85 + 0.15);
  }
  glEnd();
  glColor3f(options.line_color.g, options.line_color.b, options.line_color.r);
  glBegin(GL_LINE_STRIP);
  for (float ti=0; ti<=1; ti+=TSTEP) {
    float temp = psi0(ti);
    glVertex2d(ti, temp * 0.85 + 0.15);
  }
  glEnd();
  glColor3f(options.line_color.b, options.line_color.r, options.line_color.g);
  glBegin(GL_LINE_STRIP);
  for (float ti=0; ti<=1; ti+=TSTEP) {
    float temp = phi1(ti);
    glVertex2d(ti, temp * 0.85 + 0.15);
  }
  glEnd();
  glColor3f(options.line_color.g, options.line_color.r, options.line_color.b);
  glBegin(GL_LINE_STRIP);
  for (float ti=0; ti<=1; ti+=TSTEP) {
    float temp = psi1(ti);
    glVertex2d(ti, temp * 0.85 + 0.15);
  }
  glEnd();
}

void bezier_base() {
  float** B = new float*[TSAMPLES];
  std::vector<float> w_t(TSAMPLES, 0.0f);
  std::vector<float> w_p(points.size());

  for (size_t i=0; i<points.size(); i++) {
    w_p[i] = points[i].weight;
  }

  float ti = 0;
  for (int k=0; k<TSAMPLES; k++, ti+=TSTEP) {
    B[k] = new float[points.size() + 1]();
    B[k][points.size()] = 1.0f;  // condizione iniziale

    for (size_t i=1; i<points.size(); i++) {
      for (size_t j=0; j<points.size()+1; j++) {
        if (j > 0) {
          B[k][j] *= ti;
        }
        if (j < points.size()) {
          B[k][j] += (1 - ti) * B[k][j+1];
        }
      }
    }
  }

  for (int k=0; k<TSAMPLES; k++) {
    for (size_t j=0; j<points.size(); j++) {
      w_t[k] += B[k][j+1] * w_p[j];
    }
  }

  for (size_t i=0; i<points.size(); i++) {
    glBegin(GL_LINE_STRIP);
    for (int k=0; k<TSAMPLES; k++) {
      glVertex2f(TSTEP * k, w_p[i] * B[k][i+1] / w_t[k]);
    }
    glEnd();
  }

  // Libera la memoria occupata dalla matrice B
  for (int k=0; k<TSAMPLES; k++) {
    delete[] B[k];
  }
  delete[] B;
}

void subdivision() {
  std::vector<GLPoint2D> c(points.size());
  std::vector<GLPoint2D> subd(2 * (points.size() - 1));

  for (size_t i=0; i<points.size(); i++) {
    c[i].x = points[i].x;
    c[i].y = points[i].y;
  }

  GLPoint2D bezier_point;
  for (size_t i=0; i+1<points.size(); i++) {
    subd[i] = c.front();
    subd[subd.size() - i - 1] = c[points.size() - i - 1];
    for (size_t j=0; j+i+1<points.size(); j++) {
      c[j] = c[j] * (1 - options.subdiv) + c[j+1] * options.subdiv;
    }
    bezier_point = c[0];
  }

  // Disegna i segmenti che "visualizzano" la suddivisione
  glColor3f(SUBDIVISION_COLOR.r, SUBDIVISION_COLOR.g, SUBDIVISION_COLOR.b);
  //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  glLineWidth(2);
  glBegin(GL_LINE_STRIP);
  for (auto p: subd) {
    glVertex2f(p.x, p.y);
  }
  glEnd();

  // Evidenzia il punto finale, che fa parte della curva di Bezier
  glPointSize(5);
  glBegin(GL_POINTS);
    glVertex2f(bezier_point.x, bezier_point.y);
  glEnd();
}

void degree_elevation() {
  // Copiamo i punti in un array ausiliario
  elevated = points;

  // Eseguiamo tante volte la degree elevation
  for (int k=0; k<options.degree; k++) {
    std::vector<ControlPoint> c(elevated);
    c.push_back(c.back());

    // Troviamo i nuovi punti
    for (size_t i=c.size()-2; i>0; i--) {
      float ti = i * 1.0f / (c.size() - 1);
      c[i] = c[i-1] * ti + c[i] * (1 - ti);
    }

    // Disegnamoli, intanto
    GLColor3f d_color = {
      DEGREE_COLOR.r + (CURVE_COLOR.r - DEGREE_COLOR.r) / MAX_DEGREE * k,
      DEGREE_COLOR.g + (CURVE_COLOR.g - DEGREE_COLOR.g) / MAX_DEGREE * k,
      DEGREE_COLOR.b + (CURVE_COLOR.b - DEGREE_COLOR.b) / MAX_DEGREE * k
    };
    glColor3f(d_color.r, d_color.g, d_color.b);
    //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
    glLineWidth(1);
    glBegin(GL_LINE_STRIP);
    for (auto p: c) {
      glVertex2f(p.x, p.y);
    }
    glEnd();

    // Ora i punti sono quelli ottenuti dalla suddivisione
    elevated = c;
  }
}

size_t get_point(int idx) {
  if (idx < 0) {
    return 0;
  }
  if (idx >= (int)points.size()) {
    return points.size() - 1;
  }
  return idx;
}

void de_boor() {
  const int m = MAX_MULT;
  std::vector<GLPoint2D> c(m);
  std::vector<float> w(points.size());

  // Prepara un array points2 con i punti duplicati (quando necessario)
  std::vector<ControlPoint> points2;
  for (auto p: points) {
    for (int i=0; i<p.mult; i++) {
      points2.push_back(p);
    }
  }

  //glColor3f(CURVE_COLOR.r, CURVE_COLOR.g, CURVE_COLOR.b);
  glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for (float vt=TSTEP; vt<=1; vt+=TSTEP) {
    size_t l;
    for (size_t i=0; i<points2.size(); i++) {
      if (vt > points2[i].t - EPSILON && vt < points2[i+1].t + EPSILON) {
        l = i;
        break;
      }
    }

    // Algoritmo di De Boor
    for (int i=0; i<m; i++) {
      w[i] = points[get_point(i + l - m + 2)].weight;
      c[i] = points[get_point(i + l - m + 2)] * w[i];
    }

    for (int j=0; j<m-1; j++) {
      for (int i=m-1; i>j; i--) {
        int ti = get_point(l - m + 1 + i);
        int timj = get_point(l + i - j);
        float den = points2[timj].t - points2[ti].t;
        float dt = (vt - points2[ti].t) / den;
        c[i] = c[i] * dt + c[i-1] * (1 - dt);
        w[i] = w[i] * dt + w[i-1] * (1 - dt);
      }
    }

    glVertex2f(c[m-1].x / w[m-1], c[m-1].y / w[m-1]);
  }
  glEnd();
}

void spline_base() {
  const int m = MAX_MULT;
  std::vector<float> w_t(TSAMPLES, 0.0f);
  std::vector<float> w_p(points.size());

  for (size_t i=0; i<points.size(); i++) {
    w_p[i] = points[i].weight;
  }

  float **B = new float*[points.size()];
  for (size_t i=0; i<points.size(); i++) {
    B[i] = new float[TSAMPLES]();
  }

  // Valutazione di ciascuna funzione base per ogni valore del parametro t mediande le formule di Cox
  float ti = 0;
  for (int k=0; k<TSAMPLES; k++, ti+=TSTEP) {
    int l = localizza_intervallo_internodale(ti);
    // Costruisco la mia matrice per righe
    B[l][k] = 1;
    for (int i=0; i<m-1 ; i++) {
      float tmp = 0;
      for (int j=l-i; j<=l; j++) {
        float d1 = ti - points[get_point(j)].t;
        float d2 = points[get_point(i+j+1)].t - ti;
        float beta = B[get_point(j)][k] / (d1 + d2);
        B[get_point(j-1)][k] = d2 * beta + tmp;
        tmp = d1 * beta;
      }
      B[l][k] = tmp;
    }
  }

  for (int k=0; k<TSAMPLES; k++) {
    for (size_t j=0; j<points.size(); j++) {
      w_t[k] += B[j][k] * w_p[j];
    }
  }

  for (size_t i=0; i<points.size(); i++) {
    glBegin(GL_LINE_STRIP);
    float ti = 0;
    for (int k=0; k<TSAMPLES; k++, ti+=TSTEP) {
      glVertex2f(ti, w_p[i] * B[i][k] / w_t[k]);
    }
    glEnd();
  }

  for (size_t j=0; j<points.size(); j++) {
    glColor3f(MULTIPLICITY_COLOR.r, MULTIPLICITY_COLOR.g, MULTIPLICITY_COLOR.b);
    //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
    glRasterPos2f(points[j].t, 0);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, '0' + points[j].mult);
  }

  // Libera la memoria occupata dalla matrice B
  for (size_t i=0; i<points.size(); i++) {
    delete[] B[i];
  }
  delete[] B;
}

void choose_method(int scelta) {
  options.method = scelta;

  trigger_redisplay();
}

void choose_degree_elevation(int scelta) {
  if (scelta == 1) {  // applica
    points = elevated;
    elevated.clear();
    selected = insert_before = points.end();
  } else {  // azzera
    spinner_deg->set_int_val(0);
  }

  trigger_redisplay();
}

void display_basi() {
  glClearColor(options.bg_color.r, options.bg_color.g, options.bg_color.b, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(BASE_COLOR.r, BASE_COLOR.g, BASE_COLOR.b);
  //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  if (!points.empty()) {
    switch (options.method) {
    case 1:
      hermite_base();
      break;
    case 2:
      bezier_base();
      break;
    case 3:
      spline_base();
      break;
    }
  }
  glFlush();
}

void display() {
  glClearColor(options.bg_color.r, options.bg_color.g, options.bg_color.b, 0.0);
  glClear(GL_COLOR_BUFFER_BIT);

  if (points.empty()) {
    spinner_index->set_int_limits(0, 0);
    return;
  }

  // Aggiusta la molteplicita'
  for (auto& p: points) {
    p.mult = 1;
  }
  //points.back().mult = MAX_MULT;
  //points.front().mult = MAX_MULT;
  if (options.edit_mult) {
    points.at(options.mult_index).mult = options.new_mult;
    glColor3f(MULT_POINT_COLOR.r, MULT_POINT_COLOR.g, MULT_POINT_COLOR.b);
    //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
    glPushMatrix();
    glTranslatef(points.at(options.mult_index).x, points.at(options.mult_index).y, 0);
    GLUquadric *quad = gluNewQuadric();
    gluDisk(quad, 0, TOLERANCE, 8, 8);
    glPopMatrix();
  }
  spinner_index->set_int_limits(0, points.size() - 1);

  // Se c'e' un punto selezionato, evidenzialo
  if (selected != points.end()) {
    glColor3f(SELECTED_POINT_COLOR.r, SELECTED_POINT_COLOR.g, SELECTED_POINT_COLOR.b);
    //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
    glPushMatrix();
    glTranslatef(selected->x, selected->y, 0);
    GLUquadric *quad = gluNewQuadric();
    gluDisk(quad, 0, TOLERANCE, 8, 8);
    glPopMatrix();
  } else if (insert_before != points.end()) {
    glColor3f(INSERT_POINT_COLOR.r, INSERT_POINT_COLOR.g, INSERT_POINT_COLOR.b);
    //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
    glPushMatrix();
    glTranslatef(insert_point.x, insert_point.y, 0);
    GLUquadric *quad = gluNewQuadric();
    gluDisk(quad, 0, TOLERANCE, 8, 8);
    glPopMatrix();
  }

  // Disegna i lati del poligono di controllo
  //glColor3f(CONTROL_LINE_COLOR.r, CONTROL_LINE_COLOR.g, CONTROL_LINE_COLOR.b);
  glColor3f(options.line_color.b, options.line_color.r, options.line_color.g);
  glLineWidth(1);
  glBegin(GL_LINE_STRIP);
  for (auto p: points) {
    glVertex2f(p.x, p.y);
  }
  glEnd();

  // Disegna i vertici del poligono di controllo
  glColor3f(POINT_COLOR.r, POINT_COLOR.g, POINT_COLOR.b);
  //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
  glPointSize(3);
  glBegin(GL_POINTS);
  for (auto p: points) {
    glVertex2f(p.x, p.y);
  }
  glEnd();

  if (points.size() > 1) {
    // Prepara il vector con i valori dei parametri per ciascun punto
    if (options.param == 0) {
      uniform_parametrization();
    } else if (options.param == 1) {
      chord_parametrization();
    }

    // Esegui l'interpolazione scelta
    switch (options.method) {
    case 1:
      hermite_interpolation();
      break;
    case 2:
      bezier();
      if (options.show_subdiv) {
        subdivision();
      }
      degree_elevation();
      break;
    case 3:
      de_boor();
    }
  }

  // Disegna il segmento che indica quanto stiamo modificando il delta del mouse
  if (selected != points.end()) {
    glColor3f(DELTA_HELPER_COLOR.r, DELTA_HELPER_COLOR.g, DELTA_HELPER_COLOR.b);
    //glColor3f(options.line_color.r, options.line_color.g, options.line_color.b);
    glLineWidth(2);
    glBegin(GL_LINE_STRIP);
      if (options.method == 1) {
        // Il segmento rappresenta la nuova derivata
        glVertex2f(selected->x - selected->delta.x, selected->y - selected->delta.y);
        glVertex2f(selected->x + selected->delta.x, selected->y + selected->delta.y);
      } else {
        // Il segmento rappresenta il nuovo peso
        glVertex2f(selected->x, selected->y);
        glVertex2f(selected->x, selected->y + std::max(selected->delta.y, 0.0f));
      }
    glEnd();
  }

  glFlush();
}

void key_down(int key, int x, int y) {
  assert(x != y || x == y);  // no warning
  const float step = TOLERANCE;

  std::cerr << "key_down: " << key << std::endl;

  for (auto& p: points) {
    switch (key) {
    case 100:  // freccia sinistra
      p.x -= step;
      break;
    case 101:  // freccia su'
      p.y += step;
      break;
    case 102:  // freccia destra
      p.x += step;
      break;
    case 103:  // freccia giu'
      p.y -= step;
      break;
    }
  }

  trigger_redisplay();
}

void init_window(float start_x, float end_x, float start_y, float end_y) {
  glMatrixMode(GL_PROJECTION);  // definisce il sistema di coordinate
  glLoadIdentity();
  gluOrtho2D(start_x, end_x, start_y, end_y);
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  WINDOW_WIDTH  = 0.3 * glutGet(GLUT_SCREEN_WIDTH);
  WINDOW_HEIGHT = 0.6 * glutGet(GLUT_SCREEN_HEIGHT);

  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutInitWindowPosition(0, 0);
  finestra = glutCreateWindow("Curve 2D");
  init_window(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT);

  glutDisplayFunc(display);
  glutMouseFunc(mouse_click);
  glutMotionFunc(mouse_move);
  glutPassiveMotionFunc(mouse_move);
  glutSpecialFunc(key_down);

  glutInitWindowPosition(600, 0);
  basi = glutCreateWindow("Visualizzatore basi");
  init_window(0, 1, 0, 1);

  glutDisplayFunc(display_basi);
  GLUI* glui = GLUI_Master.create_glui("Opzioni");
  
  GLUI_Panel *colorPrim_panel = glui->add_panel("Colore Primitive", GLUI_PANEL_EMBOSSED);
  GLUI_Spinner *spinner_pr = glui->add_spinner_to_panel(colorPrim_panel, "R", GLUI_SPINNER_FLOAT, &options.line_color.r);
  GLUI_Spinner *spinner_pg = glui->add_spinner_to_panel(colorPrim_panel, "G", GLUI_SPINNER_FLOAT, &options.line_color.g);
  GLUI_Spinner *spinner_pb = glui->add_spinner_to_panel(colorPrim_panel, "B", GLUI_SPINNER_FLOAT, &options.line_color.b);
  spinner_pr->set_int_limits(0, 1);
  spinner_pg->set_int_limits(0, 1);
  spinner_pb->set_int_limits(0, 1);
  spinner_pr->set_float_val(0);
  spinner_pg->set_float_val(0.8);
  spinner_pb->set_float_val(0.2);
  
  GLUI_Panel *colorSfondo_panel = glui->add_panel("Colore Sfondo", GLUI_PANEL_EMBOSSED);
  GLUI_Spinner *spinner_sr = glui->add_spinner_to_panel(colorSfondo_panel, "R", GLUI_SPINNER_FLOAT, &options.bg_color.r);
  GLUI_Spinner *spinner_sg = glui->add_spinner_to_panel(colorSfondo_panel, "G", GLUI_SPINNER_FLOAT, &options.bg_color.g);
  GLUI_Spinner *spinner_sb = glui->add_spinner_to_panel(colorSfondo_panel, "B", GLUI_SPINNER_FLOAT, &options.bg_color.b);
  spinner_sr->set_int_limits(0, 1);
  spinner_sg->set_int_limits(0, 1);
  spinner_sb->set_int_limits(0, 1);
  spinner_sr->set_float_val(1);
  spinner_sg->set_float_val(1);
  spinner_sb->set_float_val(1);
    
  GLUI_Panel *actions_panel = glui->add_panel("Azioni", GLUI_PANEL_EMBOSSED);
  glui->add_button_to_panel(actions_panel, "Ripulisci", 1, clear_points);
  glui->add_button_to_panel(actions_panel, "Resetta derivate/pesi", 1, reset_deltas);

  GLUI_Panel *param_panel = glui->add_panel("Parametrizzazione", GLUI_PANEL_EMBOSSED);
  GLUI_RadioGroup *param_radio = glui->add_radiogroup_to_panel(param_panel, &options.param);
  glui->add_radiobutton_to_group(param_radio, "Uniforme");
  glui->add_radiobutton_to_group(param_radio, "Corde");

  GLUI_Panel *hermite_panel = glui->add_panel("Hermite", GLUI_PANEL_EMBOSSED);
  glui->add_button_to_panel(hermite_panel, "Hermite", 1, choose_method);

  GLUI_Panel *bezier_panel = glui->add_panel("Bezier", GLUI_PANEL_EMBOSSED);
  glui->add_button_to_panel(bezier_panel, "Bezier", 2, choose_method);
  glui->add_checkbox_to_panel(bezier_panel, "Mostra subdivision", &options.show_subdiv);
  GLUI_Spinner *spinner_subd = glui->add_spinner_to_panel(bezier_panel, "Subdivision", GLUI_SPINNER_FLOAT, &options.subdiv);
  spinner_subd->set_float_limits(0.0f, 1.0f, GLUI_LIMIT_CLAMP);
  spinner_subd->set_float_val(0.5f);
  spinner_deg = glui->add_spinner_to_panel(bezier_panel, "Degree elevation", GLUI_SPINNER_INT, &options.degree);
  spinner_deg->set_int_limits(0, MAX_DEGREE, GLUI_LIMIT_CLAMP);
  spinner_deg->set_int_val(0);
  glui->add_button_to_panel(bezier_panel, "applica deg. elev.", 1, choose_degree_elevation);
  glui->add_button_to_panel(bezier_panel, "azzera deg. elev.", 2, choose_degree_elevation);

  GLUI_Panel *spline_panel = glui->add_panel("Spline", GLUI_PANEL_EMBOSSED);
  glui->add_button_to_panel(spline_panel, "Spline", 3, choose_method);
  glui->add_checkbox_to_panel(spline_panel, "Modifica molteplicita'", &options.edit_mult);
  spinner_index = glui->add_spinner_to_panel(spline_panel, "Indice del nodo", GLUI_SPINNER_INT, &options.mult_index);
  spinner_mult = glui->add_spinner_to_panel(spline_panel, "Nuova molteplicita'", GLUI_SPINNER_INT, &options.new_mult);
  spinner_mult->set_int_limits(1, MAX_MULT, GLUI_LIMIT_CLAMP);
  
  glui->set_main_gfx_window(finestra);  // fa redisplay quando si clicca su una checkbox
  glutMainLoop();
}
