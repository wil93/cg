#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

const int screenWidth  = 640;
const int screenHeight = 480;

void draw()
{
  glClear(GL_COLOR_BUFFER_BIT);
  //~ checkerboard(80);
  glFlush();
}

void keyDown(int key, int x, int y) {
  printf("%d %d\n", x, y);
  printf("down: %d\n", key);
}

void keyUp(int key, int x, int y) {
  printf("%d %d\n", x, y);
  printf("up: %d\n", key);
}

void keyboard(unsigned char key, int x, int y) {
  printf("%d %d\n", x, y);
  printf("kb: %c\n", key);
}

void myinit() {
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glLineWidth(2.0);
  glMatrixMode(GL_PROJECTION);  // definisce il sistema di coordinate
  glLoadIdentity();
  gluOrtho2D(0.0, 640.0, 0.0, 480.0);
  glutSpecialFunc(keyDown);
  glutSpecialUpFunc(keyUp);
  glutKeyboardFunc(keyboard);
}

int main(int argc, char** argv) {
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(screenWidth, screenHeight);
  glutInitWindowPosition(100, 150);
  glutCreateWindow("Giochino");
  glutDisplayFunc(draw);
  myinit();
  glutMainLoop();
}
