clean:
	rm -f curve

curve:
	g++ -O0 -g -Wall -Wextra -pedantic -std=gnu++0x -o curve curve.cpp -lGL -lGLU -lglut -lglui

all: clean curve
